function displayList(array, parent = document.body) {
    const list = document.createElement("ul");
  
    array.forEach(item => {
      const listItem = document.createElement("li");
      listItem.textContent = item;
      list.appendChild(listItem);
    });
  
    parent.appendChild(list);
  }
  
  const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  displayList(array1);
  
  const array2 = ["1", "2", "3", "sea", "user", 23];
  const parentElement = document.getElementById("taskContainer");
  displayList(array2, parentElement);